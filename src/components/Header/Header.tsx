import { Link } from 'gatsby'
import React from 'react'
import { useLocation } from "@reach/router";
import { motion } from "framer-motion";


const headerLinks = [
    {
        title: "Home",
        url: "/"
    },
    {
        title: "Posts",
        url: "/posts/"
    }
]





function Header() {
    const location = useLocation().pathname;
    return (
        <header className='header w-full'>
            <section className='container mx-auto min-h-[70px] bg-white flex items-center'>
                <ul className='flex gap-8'>
                    {
                        headerLinks.map((item, i) => (
                            <motion.li
                                key={item.title}
                                className={`px-3 py-1 relative ${location === item.url && " text-white"}`}>
                                {location === item.url &&
                                    (<motion.div
                                        layoutId='active-header-link'
                                        transition={{
                                            type: "spring",
                                            damping: 5,
                                            stiffness: 100,
                                            restDelta: 1,
                                            bounce: 0.25,
                                            restSpeed: 1,
                                            duration: 5
                                        }}
                                        className='absolute w-full h-full bottom-0 left-0 bg-black rounded-full z-0 '>
                                    </motion.div>)
                                }
                                <Link className='z-10 relative' to={item.url}>{item.title}</Link>
                            </motion.li>
                        ))
                    }
                </ul>
            </section>
        </header>
    )
}

export default Header