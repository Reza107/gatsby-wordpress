import React from 'react'
import { Link } from 'gatsby'
import { GatsbyImage, StaticImage } from 'gatsby-plugin-image'


const PostCard = ({ data }: { data: any }) => {

    console.log(data)

    return (
        <>

            <div className="bg-white w-[300px] shadow-md hover:shadow-xl duration-500 rounded-xl">

                <Link to={`/posts/${data.slug}`}>
                    {
                        data.featuredImage ?
                            <GatsbyImage objectFit={"cover"} image={data.featuredImage.node.gatsbyImage} alt={data.title} className='rounded-2xl !w-full' />
                            :
                            <StaticImage height={200} width={300} src='../../images/image-cover.jpg' alt='no-image' className='rounded-2xl !w-full' />
                    }

                </Link>
                <div className="px-4 py-3 w-72">
                    <span className="text-gray-400 mr-3 uppercase text-xs">{
                        data.author.node.name
                    }</span>
                    <p className="text-lg font-bold text-black truncate block capitalize">
                        {data.title}
                    </p>
                    <div className="flex items-center">
                        <p className="text-sm text-gray-600 cursor-auto ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, et.</p>


                    </div>
                    <div className='w-full flex justify-center mt-5 mb-2'>
                        <Link
                            to={`/posts/${data.slug}`}
                            className='py-1 px-3 rounded-md bg-teal-400 text-white hover:bg-teal-600 transition'>
                            Read more
                        </Link>
                    </div>
                </div>
            </div>

        </>
    )
}

export default PostCard