import React, { ReactNode } from 'react'
import Header from '../components/Header/Header'
function MainLayout({ children }: { children: ReactNode }) {
    return (

        <div className='min-h-screen'>
            
            <Header />
            <main className='container mx-auto'>
                {children}
            </main>
        </div>

    )
}

export default MainLayout