import React, { ReactNode } from 'react'
import AnimatedCursor from "react-animated-cursor"
function Layout({ children }: { children: ReactNode }) {
    return (
        <>
            <AnimatedCursor
                outerStyle={{
                    border: '1px solid rgba(0,0,0,1)',
                    backgroundColor: 'transparent'
                }}
                innerStyle={{
                    backgroundColor: 'rgba(0,0,0,1)'
                }}
            />
            {children}
        </>
    )
}

export default Layout