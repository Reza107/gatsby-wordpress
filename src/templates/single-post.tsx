import { GatsbyImage, StaticImage } from 'gatsby-plugin-image'
import React from 'react'
import MainLayout from '../layouts/MainLayout'



function SinglePost({ pageContext }: { pageContext: any }) {
    const { post } = pageContext
    return (
        <MainLayout>
            <section className='container mx-auto'>
                <div className='flex flex-col gap-5'>
                    <div className='text-3xl my-4'>
                        Name: {post.title}
                    </div>

                    {
                        post.featuredImage &&
                        <div className=''>
                            <GatsbyImage objectFit={"cover"} image={post.featuredImage.node.gatsbyImage} alt={post.title} />
                        </div>
                    }


                    <div dangerouslySetInnerHTML={{ __html: post.content }}>
                    </div>

                </div>
            </section>
        </MainLayout>

    )
}

export default SinglePost