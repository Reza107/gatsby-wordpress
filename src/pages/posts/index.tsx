import React from 'react'
import { graphql, type HeadFC, type PageProps, useStaticQuery } from "gatsby"
import MainLayout from '../../layouts/MainLayout'
import PostCard from '../../components/Cards/PostCard'

const PostsPage: React.FC<PageProps> = () => {

  const FetchedData = useStaticQuery(allPosts)

  return (
    <MainLayout>
      <section className="flex w-full flex-col mt-10">
        <h1 className="text-3xl font-bold mb-10">Posts</h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 gap-16 ">
          {
            FetchedData.allWpPost.nodes.map((item: any) => (
              <PostCard key={item.id} data={item} />
            ))
          }


        </div>
      </section>
    </MainLayout>
  )
}


export default PostsPage

export const Head: HeadFC = () => <title>Posts</title>



export const allPosts =  graphql`
query allPostCards {
  allWpPost {
    nodes {
      id
      slug
      title
      author {
        node {
          name
        }
      }
      featuredImage {
        node {
          id
          gatsbyImage(width: 300,  height:200 , formats: AVIF)
        }
      }
    }
  }
}
`