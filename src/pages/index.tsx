import * as React from "react"
import type { HeadFC, PageProps } from "gatsby"
import MainLayout from "../layouts/MainLayout"
import PostCard from "../components/Cards/PostCard"
import { StaticImage } from "gatsby-plugin-image"


const IndexPage: React.FC<PageProps> = () => {
  return (
    <MainLayout>
      <h1>Home</h1>
      <section className="flex flex-wrap gap-5">

        <StaticImage height={400} width={300} src='../images/image-cover.jpg' alt='no-image' className='rounded-2xl' />
        <StaticImage height={400} width={300} src='../images/1.jpg' alt='no-image' className='rounded-2xl' />
        <StaticImage height={400} width={300} src='../images/2.jpg' alt='no-image' className='rounded-2xl' />
        <StaticImage height={400} width={300} src='../images/3.jpg' alt='no-image' className='rounded-2xl' />
      </section>

    </MainLayout>
  )
}

export default IndexPage

export const Head: HeadFC = () => <title>Home Page</title>
