const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions
    const queryResults = await graphql(`
    query AllPosts {
        allWpPost {
            nodes {
            id
            slug
            title
            featuredImage {
                node {
                id
                gatsbyImage(width: 400)
                }
            }
            content
            }
        }    
    }`)




    const postTemplate = path.resolve(`src/templates/single-post.tsx`)
    queryResults.data.allWpPost.nodes.forEach(node => {
        createPage({
            path: `/posts/${node.slug}`,
            component: postTemplate,
            context: {
                post: node,
            },
        })
    })
}